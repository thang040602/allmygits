namespace AllMyGits.Presentation.Api;

using AllMyGits.Application;
using AllMyGits.Core.ValueObjects;
using AllMyGits.Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
using Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        var dbContextConfiguration = DbContextConfiguration.GetConfiguration(builder.Configuration);
        var emailClientConfiguration = EmailClientConfiguration.GetConfiguration(builder.Configuration);
        var githubClientConfiguration = GithubClientConfiguration.GetConfiguration(builder.Configuration);
        var jwtConfiguration = JwtConfiguration.GetConfiguration(builder.Configuration);
        var domainNameConfiguration = DomainNameConfiguration.GetConfiguration(builder.Configuration);

        // Add configurations
        builder.Services.AddSingleton(dbContextConfiguration);
        builder.Services.AddSingleton(emailClientConfiguration);
        builder.Services.AddSingleton(githubClientConfiguration);
        builder.Services.AddSingleton(jwtConfiguration);
        builder.Services.AddSingleton(domainNameConfiguration);

        // Add services to the container
        builder.Services.AddControllers();
        builder.Services.AddInfrastructures(dbContextConfiguration);
        builder.Services.AddServices();

        builder.Services.AddCors(options =>
        {
            options.AddPolicy("AnyClient", builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
        });

        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();

        builder.Services.AddSwaggerGen(option =>
        {
            option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Description = "Please enter a valid token",
                Name = "Authorization",
                Type = SecuritySchemeType.Http,
                BearerFormat = "JWT",
                Scheme = "Bearer"
            });
            option.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] {}
                }
            });
        });

        builder.Services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.RequireHttpsMetadata = false;
            options.SaveToken = true;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidIssuer = jwtConfiguration.Issuer,
                ValidAudience = jwtConfiguration.Audience,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfiguration.Secret)),
                ValidateLifetime = true
            };
        });


        builder.Services.AddDataProtection().UseCryptographicAlgorithms(new AuthenticatedEncryptorConfiguration
        {
            EncryptionAlgorithm = EncryptionAlgorithm.AES_256_CBC,
            ValidationAlgorithm = ValidationAlgorithm.HMACSHA256
        });

        builder.Services.AddLogging(options =>
        {
            options.ClearProviders();
            options.AddConsole();
        });

        builder.Services.AddHealthChecks();

        var app = builder.Build();

        // Configure the HTTP request pipeline
        app.UseSwagger();
        app.UseSwaggerUI();
        app.UseHttpsRedirection();
        app.UseAuthentication();
        app.UseAuthorization();
        app.MapControllers();
        app.UseHealthChecks("/HealthCheck");
        app.UseHttpLogging();
        app.UseCors("AnyClient");

        app.Run();
    }
}