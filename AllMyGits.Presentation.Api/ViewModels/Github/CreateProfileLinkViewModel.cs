﻿namespace AllMyGits.Presentation.Api.ViewModels.Github;

public class CreateProfileLinkViewModel
{
    public string UserName { get; set; }
}
