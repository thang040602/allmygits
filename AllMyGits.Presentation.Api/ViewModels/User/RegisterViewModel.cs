﻿namespace AllMyGits.Presentation.Api.ViewModels.User;

using System.ComponentModel.DataAnnotations;

public class RegisterViewModel
{
    [Required]
    public string UserName { get; set; }

    [Required]
    [DataType(DataType.Password)]
    public string Password { get; set; }

    [Required]
    [DataType(DataType.EmailAddress)]
    public string Email { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }
}
