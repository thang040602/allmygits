﻿namespace AllMyGits.Presentation.Api.ViewModels.User;

public class AuthenticateViewModel
{
    public string UserNameOrEmail { get; set; }
    public string Password { get; set; }
}
