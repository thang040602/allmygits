﻿namespace AllMyGits.Presentation.Api.ViewModels.User;

public class ResetPasswordViewModel
{
    public string Id { get; set; }
    public string Token { get; set; }
    public string NewPassword { get; set; }
}
