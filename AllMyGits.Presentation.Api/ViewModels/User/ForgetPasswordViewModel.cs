﻿namespace AllMyGits.Presentation.Api.ViewModels.User;

public class ForgetPasswordViewModel
{
    public string Email { get; set; }
    public string CallbackUrl { get; set; }
}
