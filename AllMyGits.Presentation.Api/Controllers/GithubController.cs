﻿namespace AllMyGits.Presentation.Api.Controllers;

using AllMyGits.Core.Interfaces.Services;
using AllMyGits.Presentation.Api.ViewModels.Github;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

[Authorize]
[ApiController]
[Route("/api/v1/[controller]")]
public class GithubController : Controller
{
    private readonly IGithubProfileLinkService _githubProfileLinkService;

    public GithubController(IGithubProfileLinkService githubProfileLinkService)
    {
        _githubProfileLinkService = githubProfileLinkService;
    }

    [HttpPost]
    public async Task<IActionResult> CreateProfileLinkAsync(CreateProfileLinkViewModel createProfileLinkViewModel)
    {
        string id = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier, StringComparison.Ordinal))!.Value;
        await _githubProfileLinkService.CreateAsync(id, createProfileLinkViewModel.UserName);
        return Ok();
    }

    [HttpDelete]
    public async Task<IActionResult> DeleteProfileLinkAsync(string id)
    {
        await _githubProfileLinkService.DeleteAsync(await _githubProfileLinkService.GetById(id));
        return Ok();
    }
}
