﻿namespace AllMyGits.Presentation.Api.Controllers;

using AllMyGits.Core.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

[Authorize]
[ApiController]
[Route("/api/v1/[controller]")]
public class PrivateDashboardController : Controller
{
    private readonly IGithubProfileLinkService _githubProfileLinkService;

    public PrivateDashboardController(IGithubProfileLinkService githubProfileLinkService)
    {
        _githubProfileLinkService = githubProfileLinkService;
    }

    [HttpGet]
    public async Task<IEnumerable<dynamic>> GetDashboardAsync()
    {
        string id = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier, StringComparison.Ordinal))!.Value;
        return await _githubProfileLinkService.GetRepositories(id);
    }
}
