﻿namespace AllMyGits.Presentation.Api.Controllers;

using AllMyGits.Core.Interfaces.Services;
using AllMyGits.Presentation.Api.ViewModels.User;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("/api/v1/[controller]")]
public class UserController : Controller
{
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
        _userService = userService;
    }

    [HttpPost]
    public async Task<IActionResult> RegisterAsync([FromBody] RegisterViewModel registerViewModel)
    {
        await _userService.AddAsync(registerViewModel.UserName, registerViewModel.Email, registerViewModel.Password, registerViewModel.FirstName, registerViewModel.LastName);
        return Ok("New user is registered.");
    }

    [HttpGet("ConfirmEmail")]
    public async Task<IActionResult> ConfirmEmailAsync(string id, string token)
    {
        var user = await _userService.FindByIdAsync(id);

        if (user is not null)
        {
            var result = await _userService.ConfirmEmailAsync(id, token);
            return result ? Ok("Email is confirmed.") : BadRequest();
        }

        return Ok();
    }

    [HttpPost("Authenticate")]
    public async Task<IActionResult> AuthenticateAsync([FromBody] AuthenticateViewModel authenticateViewModel)
    {
        return Ok(await _userService.AuthenticateAsync(authenticateViewModel.UserNameOrEmail, authenticateViewModel.Password));
    }

    [HttpPost("ForgetPassword")]
    public async Task<IActionResult> ForgetPasswordAsync([FromBody] ForgetPasswordViewModel forgetPasswordViewModel)
    {
        await _userService.ForgetPasswordAsync(forgetPasswordViewModel.Email, forgetPasswordViewModel.CallbackUrl);
        return Ok("Password reset email is sent.");
    }

    [HttpPost("ResetPassword")]
    public async Task<IActionResult> ResetPasswordAsync([FromBody] ResetPasswordViewModel resetPasswordViewModel)
    {
        await _userService.ResetPasswordAsync(resetPasswordViewModel.Id, resetPasswordViewModel.NewPassword, resetPasswordViewModel.Token);
        return Ok("Password is reseted.");
    }
}
