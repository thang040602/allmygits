﻿namespace AllMyGits.Presentation.Web.Providers;

using AllMyGits.Presentation.Web.Utilities;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;

public class CustomAuthenticationStateProvider : AuthenticationStateProvider
{
    private readonly ILocalStorageService _localStorageService;

    public CustomAuthenticationStateProvider(ILocalStorageService localStorageService)
    {
        _localStorageService = localStorageService;
    }

    public override async Task<AuthenticationState> GetAuthenticationStateAsync()
    {
        string token = await _localStorageService.GetItemAsStringAsync("authToken");

        var identity = new ClaimsIdentity();

        if (!string.IsNullOrEmpty(token))
        {
            identity = new ClaimsIdentity(JwtParser.ParseClaimsFromJwt(token), "jwt");
        }

        var principal = new ClaimsPrincipal(identity);
        var state = new AuthenticationState(principal);

        NotifyAuthenticationStateChanged(Task.FromResult(state));

        return state;
    }

    public async Task NotifyUserLoginAsync()
    {
        var state = await GetAuthenticationStateAsync();
        NotifyAuthenticationStateChanged(Task.FromResult(state));
    }
}