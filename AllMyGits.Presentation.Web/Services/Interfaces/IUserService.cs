﻿namespace AllMyGits.Presentation.Web.Services.Interfaces;

using AllMyGits.Presentation.Web.Models;

public interface IUserService
{
    Task LoginAsync(LoginRequest loginRequest);
    Task<UserModel> FindAsync(string id);
}
