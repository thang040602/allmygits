﻿namespace AllMyGits.Presentation.Web.Services;

using AllMyGits.Presentation.Web.Client;
using AllMyGits.Presentation.Web.Models;
using AllMyGits.Presentation.Web.Providers;
using Blazored.LocalStorage;
using Interfaces;
using System.Threading.Tasks;

public class UserService : IUserService
{
    private readonly ApiClient _apiClient;
    private readonly ILocalStorageService _localStorageService;
    private readonly CustomAuthenticationStateProvider _authenticationStateProvider;

    public UserService(ApiClient apiClient, ILocalStorageService localStorageService, CustomAuthenticationStateProvider authenticationStateProvider)
    {
        _apiClient = apiClient;
        _localStorageService = localStorageService;
        _authenticationStateProvider = authenticationStateProvider;
    }

    public async Task<UserModel> FindAsync(string id)
    {
        throw new NotImplementedException();
    }

    public async Task LoginAsync(LoginRequest loginRequest)
    {
        string token = await _apiClient.AuthenticateAsync(loginRequest);
        await _localStorageService.SetItemAsync("authToken", token);
        await _authenticationStateProvider.NotifyUserLoginAsync();
        _apiClient.AddAuthenticationToken(token);
    }
}
