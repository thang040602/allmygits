﻿namespace AllMyGits.Presentation.Web.Models;

public class UserModel
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string UserName { get; set; }
    public DateTime CreatedAt { get; set; }
}
