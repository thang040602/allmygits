﻿namespace AllMyGits.Presentation.Web.Client;

public class ApiConfiguration
{
    public string BaseUrl { get; init; }
    public string AuthenticateRoute { get; init; }
}
