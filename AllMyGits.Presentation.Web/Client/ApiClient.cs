﻿namespace AllMyGits.Presentation.Web.Client;

using AllMyGits.Presentation.Web.Models;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

public class ApiClient
{
    private readonly HttpClient _httpClient;
    private readonly ApiConfiguration _apiConfiguration;

    public ApiClient(ApiConfiguration apiConfiguration)
    {
        _httpClient = new HttpClient();
        _apiConfiguration = apiConfiguration;
    }

    public async Task<string> AuthenticateAsync(LoginRequest loginRequest)
    {
        var response = await _httpClient.PostAsync(BuildUrl(_apiConfiguration.AuthenticateRoute), new StringContent(JsonSerializer.Serialize(loginRequest), Encoding.UTF8, "application/json"));
        response.EnsureSuccessStatusCode();
        return await response.Content.ReadAsStringAsync();
    }

    public void AddAuthenticationToken(string token)
    {
        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
    }

    public void RemoveAuthenticationToken()
    {
        _httpClient.DefaultRequestHeaders.Authorization = null;
    }

    private string BuildUrl(string route)
    {
        return $"{_apiConfiguration.BaseUrl}{route}";
    }
}
