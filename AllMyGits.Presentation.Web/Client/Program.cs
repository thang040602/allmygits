using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

using AllMyGits.Presentation.Web.Providers;
using AllMyGits.Presentation.Web.Client;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using AllMyGits.Presentation.Web.Services.Interfaces;
using AllMyGits.Presentation.Web.Services;

namespace AllMyGits.Presentation.Web
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            var apiConfiguration = new ApiConfiguration
            {
                BaseUrl = builder.Configuration["ApiConfiguration:BaseUrl"],
                AuthenticateRoute = builder.Configuration["ApiConfiguration:AuthenticateRoute"]
            };
            builder.Services.AddSingleton(apiConfiguration);

            builder.Services.AddScoped<ApiClient>();
            builder.Services.AddScoped<AuthenticationStateProvider, CustomAuthenticationStateProvider>();
            builder.Services.AddScoped<CustomAuthenticationStateProvider>();
            builder.Services.AddScoped<IUserService, UserService>();
            builder.Services.AddBlazoredLocalStorage();
            builder.Services.AddAuthorizationCore();

            await builder.Build().RunAsync();
        }
    }
}