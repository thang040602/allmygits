﻿namespace AllMyGits.Infrastructure.Repositories;

using AllMyGits.Core.Entities;
using AllMyGits.Core.Interfaces.Repositories;

public class GithubProfileLinkRepository : BaseProfileLinkRepository<GithubProfileLink>, IGithubProfileLinkRepository
{
    public GithubProfileLinkRepository(AllMyGitsDbContext dbContext) : base(dbContext)
    {
    }
}
