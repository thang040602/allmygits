﻿namespace AllMyGits.Infrastructure.Repositories;

using AllMyGits.Core.Entities;
using AllMyGits.Core.Interfaces.Repositories;

using Microsoft.AspNetCore.Identity;

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

public class UserRepository : IUserRepository
{
    private readonly SignInManager<User> _signInManager;
    private readonly UserManager<User> _userManager;

    public UserRepository(SignInManager<User> signInManager, UserManager<User> userManager)
    {
        _signInManager = signInManager;
        _userManager = userManager;
    }

    public async Task AddAsync(User user, string password)
    {
        var result = await _userManager.CreateAsync(user, password);

        if (!result.Succeeded)
        {
            throw new SystemException(result.ToString());
        }
    }

    public async Task ChangeEmailAsync(User user, string newEmail, string token)
    {
        await _userManager.ChangeEmailAsync(user, newEmail, token);
    }

    public async Task ChangePasswordAsync(User user, string currentPassword, string newPassword)
    {
        await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
    }

    public IQueryable<User> Find(Expression<Func<User, bool>> predicate)
    {
        return _userManager.Users.Where(predicate);
    }

    public Task<string> GeneratePasswordResetTokenAsync(User user)
    {
        return _userManager.GeneratePasswordResetTokenAsync(user);
    }

    public Task<string> GenerateEmailConfirmationTokenAsync(User user)
    {
        return _userManager.GenerateEmailConfirmationTokenAsync(user);
    }

    public Task<string> GenerateChangeEmailTokenAsync(User user, string newEmail)
    {
        return _userManager.GenerateChangeEmailTokenAsync(user, newEmail);
    }

    public Task<SignInResult> PasswordSignInAsync(string userName, string password)
    {
        return _signInManager.PasswordSignInAsync(userName, password, false, false);
    }

    public async Task RemoveAsync(User user)
    {
        await _userManager.DeleteAsync(user);
    }

    public async Task UpdateAsync(User user)
    {
        await _userManager.UpdateAsync(user);
    }

    public async Task<User> FindByEmailAsync(string email)
    {
        return await _userManager.FindByEmailAsync(email);
    }

    public async Task<User> FindByUserNameAsync(string userName)
    {
        return await _userManager.FindByNameAsync(userName);
    }

    public async Task<User> FindByIdAsync(string id)
    {
        return await _userManager.FindByIdAsync(id);
    }

    public Task<IdentityResult> ConfirmEmailAsync(User user, string token)
    {
        return _userManager.ConfirmEmailAsync(user, token);
    }

    public Task<IdentityResult> ResetPasswordAsync(User user, string token, string password)
    {
        return _userManager.ResetPasswordAsync(user, token, password);
    }
}
