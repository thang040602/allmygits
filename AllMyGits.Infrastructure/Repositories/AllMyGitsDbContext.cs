﻿namespace AllMyGits.Infrastructure.Repositories;

using AllMyGits.Core.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

public class AllMyGitsDbContext : IdentityDbContext<User>
{
    public DbSet<GithubProfileLink> GithubProfileLinks { get; set; }

    public AllMyGitsDbContext(DbContextOptions<AllMyGitsDbContext> options) : base(options)
    {
    }
}
