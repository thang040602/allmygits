﻿namespace AllMyGits.Infrastructure.Repositories;

using AllMyGits.Core.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

public class BaseProfileLinkRepository<TProfileLink> : IProfileLinkRepository<TProfileLink> where TProfileLink : class
{
    protected AllMyGitsDbContext _dbContext;
    protected DbSet<TProfileLink> _dbSet;

    public BaseProfileLinkRepository(AllMyGitsDbContext dbContext)
    {
        _dbContext = dbContext;
        _dbSet = dbContext.Set<TProfileLink>();
    }

    public void Add(TProfileLink profileLink)
    {
        _dbSet.Add(profileLink);
        _dbContext.SaveChanges();
    }

    public IQueryable<TProfileLink> Find(Expression<Func<TProfileLink, bool>> predicate)
    {
        return _dbSet.Where(predicate);
    }

    public void Remove(TProfileLink profileLink)
    {
        _dbSet.Remove(profileLink);
        _dbContext.SaveChanges();
    }

    public void RemoveRange(IEnumerable<TProfileLink> profileLinks)
    {
        _dbSet.RemoveRange(profileLinks);
        _dbContext.SaveChanges();
    }

    public void Update(TProfileLink profileLink)
    {
        _dbSet.Update(profileLink);
        _dbContext.SaveChanges();
    }
}
