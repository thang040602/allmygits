﻿namespace AllMyGits.Infrastructure;

using AllMyGits.Core.Entities;
using AllMyGits.Core.Interfaces.Clients;
using AllMyGits.Core.Interfaces.Repositories;
using AllMyGits.Core.ValueObjects;
using AllMyGits.Infrastructure.Clients;
using AllMyGits.Infrastructure.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

public static class Startup
{
    public static void AddInfrastructures(this IServiceCollection services, DbContextConfiguration dbContextConfiguration)
    {
        // Add client implementations
        services.AddScoped<IEmailClient, EmailClient>();
        services.AddScoped<IGithubClient, GithubClient>();

        // Add repository implementations
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IGithubProfileLinkRepository, GithubProfileLinkRepository>();

        // Add DbContext
        services.AddDbContext<AllMyGitsDbContext>(options =>
        {
            options.UseNpgsql(GetConnectionString(dbContextConfiguration), x => x.MigrationsAssembly("AllMyGits.Infrastructure"));
        });

        // Add Identity
        services.AddIdentity<User, IdentityRole>(options =>
        {
            options.User.RequireUniqueEmail = true;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequiredUniqueChars = 0;
            options.Password.RequireDigit = false;
            options.Password.RequireUppercase = false;
            options.Password.RequireLowercase = false;
            options.Password.RequiredLength = 6;
        }).AddEntityFrameworkStores<AllMyGitsDbContext>().AddDefaultTokenProviders();
    }

    private static string GetConnectionString(DbContextConfiguration dbContextConfiguration)
    {
        return $"Host={dbContextConfiguration.Host};Database={dbContextConfiguration.DatabaseName};Username={dbContextConfiguration.UserName};Password={dbContextConfiguration.Password}";
    }
}
