﻿namespace AllMyGits.Infrastructure.Clients;

using AllMyGits.Core.DTOs;
using AllMyGits.Core.Interfaces.Clients;
using AllMyGits.Core.ValueObjects;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;

public class EmailClient : IEmailClient
{
    private EmailClientConfiguration _configuration;

    public EmailClient(EmailClientConfiguration configuration)
    {
        _configuration = configuration;
    }

    public void SendEmail(EmailDTO emailDTO)
    {
        using var smtp = new SmtpClient();
        smtp.Connect(_configuration.SmtpServer, _configuration.Port, SecureSocketOptions.StartTls);
        smtp.Authenticate(_configuration.UserName, _configuration.Password);
        smtp.Send(CreateMimeMessage(emailDTO));
        smtp.Disconnect(true);
    }

    private MimeMessage CreateMimeMessage(EmailDTO emailDTO)
    {
        var message = new MimeMessage();

        message.From.Clear();
        message.To.Clear();

        message.From.Add(new MailboxAddress(_configuration.DisplayName, _configuration.UserName));
        message.To.Add(MailboxAddress.Parse(emailDTO.Recipient));
        message.Subject = emailDTO.Subject;
        message.Body = new TextPart(TextFormat.Html)
        {
            Text = emailDTO.Body
        };

        return message;
    }
}
