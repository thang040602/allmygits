﻿namespace AllMyGits.Infrastructure.Clients;

using AllMyGits.Core.Entities;
using AllMyGits.Core.Interfaces.Clients;
using AllMyGits.Core.ValueObjects;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;

public class GithubClient : IGithubClient
{
    private readonly GithubClientConfiguration _configuration;

    public GithubClient(GithubClientConfiguration configuration)
    {
        _configuration = configuration;
    }

    public async Task<IQueryable<GithubRepository>> GetRepositoriesAsync(string userName)
    {
        var client = new RestClient(_configuration.BaseApiUrl);

        var request = new RestRequest($"/users/{userName}/repos", Method.Get);
        request.AddHeader("Accept", "application/vnd.github+json");

        var response = await client.GetAsync(request);

        if (response.IsSuccessful)
        {
            var result = JsonConvert.DeserializeObject<List<GithubRepository>>(response.Content!);
            return (result ?? new List<GithubRepository>()).AsQueryable();
        }
        else
        {
            throw new SystemException("Error occurred.");
        }
    }
}