﻿namespace AllMyGits.Application;

using AllMyGits.Application.Services;
using AllMyGits.Core.Interfaces.Services;
using Microsoft.Extensions.DependencyInjection;

public static class Startup
{
    public static void AddServices(this IServiceCollection services)
    {
        services.AddScoped<IEmailService, EmailService>();
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IGithubProfileLinkService, GithubProfileLinkService>();
    }
}
