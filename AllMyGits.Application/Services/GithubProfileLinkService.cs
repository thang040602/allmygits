﻿namespace AllMyGits.Application.Services;

using AllMyGits.Core.Entities;
using AllMyGits.Core.Interfaces.Clients;
using AllMyGits.Core.Interfaces.Repositories;
using AllMyGits.Core.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

public class GithubProfileLinkService : BaseProfileLinkService<GithubProfileLink>, IGithubProfileLinkService
{
    private readonly IGithubClient _githubClient;
    private readonly IUserRepository _userRepository;
    private readonly IGithubProfileLinkRepository _githubProfileLinkRepository;

    public GithubProfileLinkService(IGithubProfileLinkRepository githubProfileLinkRepository, IGithubClient githubClient, IUserRepository userRepository) : base(githubProfileLinkRepository)
    {
        _githubProfileLinkRepository = githubProfileLinkRepository;
        _githubClient = githubClient;
        _userRepository = userRepository;
    }

    public async Task CreateAsync(string ownerId, string githubUserName)
    {
        var owner = await _userRepository.FindByIdAsync(ownerId);
        _githubProfileLinkRepository.Add(new GithubProfileLink
        {
            Owner = owner,
            GithubUserName = githubUserName,
            CreatedAt = DateTime.Now.ToUniversalTime(),
        });
    }

    public async Task<IEnumerable<GithubRepository>> GetRepositories(string ownerId)
    {
        var profileLinks = await GetByOwnerId(ownerId);
        var taskArray = profileLinks.Select(x => _githubClient.GetRepositoriesAsync(x.GithubUserName)).ToArray();
        Task.WaitAll(taskArray);
        return taskArray.Select(x => x.Result).Aggregate(new List<GithubRepository>().AsEnumerable(), (result, x) => result.Concat(x));
    }
}
