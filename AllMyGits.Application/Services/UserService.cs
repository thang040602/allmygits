﻿namespace AllMyGits.Application.Services;

using AllMyGits.Core.Entities;
using AllMyGits.Core.Interfaces.Repositories;
using AllMyGits.Core.Interfaces.Services;
using AllMyGits.Core.ValueObjects;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;
    private readonly IEmailService _emailService;
    private readonly JwtConfiguration _jwtConfiguration;
    private readonly DomainNameConfiguration _domainNameConfiguration;

    public UserService(IUserRepository userRepository, IEmailService emailService, JwtConfiguration jwtConfiguration, DomainNameConfiguration domainNameConfiguration)
    {
        _userRepository = userRepository;
        _emailService = emailService;
        _jwtConfiguration = jwtConfiguration;
        _domainNameConfiguration = domainNameConfiguration;
    }

    public async Task AddAsync(string userName, string email, string password, string firstName = null, string lastName = null)
    {
        await _userRepository.AddAsync(new User
        {
            UserName = userName,
            Email = email,
            FirstName = firstName,
            LastName = lastName
        }, password);

        // Send confirmation email
        var user = await FindByUserNameAsync(userName);
        var token = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(await _userRepository.GenerateEmailConfirmationTokenAsync(user)));
        var callbackUrl = $"{_domainNameConfiguration.DomainName}/api/v1/User/ConfirmEmail";
        _emailService.SendConfirmationEmail(email, user.Id, callbackUrl, token);
    }

    public async Task<string> AuthenticateAsync(string userNameOrEmail, string password)
    {
        string userName = await GetUserName(userNameOrEmail);

        var result = await _userRepository.PasswordSignInAsync(userName, password);

        if (result.Succeeded)
        {
            var user = await FindByUserNameAsync(userName);
            var token = GetToken(new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Name, user.UserName)
            });
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        else
        {
            throw new UnauthorizedAccessException("Wrong identity.");
        }
    }

    public async Task<User> FindByEmailAsync(string email)
    {
        return await _userRepository.FindByEmailAsync(email);
    }

    public async Task<User> FindByIdAsync(string id)
    {
        return await _userRepository.FindByIdAsync(id);
    }

    public async Task<User> FindByUserNameAsync(string userName)
    {
        return await _userRepository.FindByUserNameAsync(userName);
    }

    public async Task RemoveAsync(string id)
    {
        await _userRepository.RemoveAsync(await FindByIdAsync(id));
    }

    public async Task UpdateAsync(string id, string? userName = null, string? firstName = null, string? lastName = null)
    {
        var user = await FindByIdAsync(id);
        if (userName is not null) user.UserName = userName;
        if (firstName is not null) user.FirstName = firstName;
        if (lastName is not null) user.LastName = lastName;

        await _userRepository.UpdateAsync(user);
    }

    public async Task<bool> ConfirmEmailAsync(string id, string token)
    {
        var user = await FindByIdAsync(id);
        var result = await _userRepository.ConfirmEmailAsync(user, Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(token)));
        return result.Succeeded;
    }

    public async Task ForgetPasswordAsync(string email, string callbackUrl)
    {
        var user = await FindByEmailAsync(email);
        string token = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(await _userRepository.GeneratePasswordResetTokenAsync(user)));
        _emailService.SendPasswordResetEmail(email, user.Id, callbackUrl, token);
    }

    public async Task ResetPasswordAsync(string id, string password, string token)
    {
        var user = await FindByIdAsync(id);
        var result = await _userRepository.ResetPasswordAsync(user, Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(token)), password);
        if (!result.Succeeded)
        {
            throw new SystemException("Resetting password was unsuccessful.");
        }
    }

    private async Task<string> GetUserName(string userNameOrEmail)
    {
        if (IsEmail(userNameOrEmail))
        {
            return (await FindByEmailAsync(userNameOrEmail)).UserName;
        }
        else
        {
            return userNameOrEmail;
        }
    }

    private bool IsEmail(string str)
    {
        try
        {
            _ = new MailAddress(str);
            return true;
        }
        catch
        {
            return false;
        }
    }

    private JwtSecurityToken GetToken(List<Claim> claims)
    {
        var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfiguration.Secret));

        var token = new JwtSecurityToken(
            issuer: _jwtConfiguration.Issuer,
            audience: _jwtConfiguration.Audience,
            expires: DateTime.Now.AddDays(_jwtConfiguration.DaysExpiration),
            claims: claims,
            signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));

        return token;
    }
}
