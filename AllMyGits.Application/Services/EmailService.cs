﻿namespace AllMyGits.Application.Services;

using AllMyGits.Core.DTOs;
using AllMyGits.Core.Interfaces.Clients;
using AllMyGits.Core.Interfaces.Services;

public class EmailService : IEmailService
{
    private readonly IEmailClient _emailClient;

    public EmailService(IEmailClient emailClient)
    {
        _emailClient = emailClient;
    }

    public void SendConfirmationEmail(string recipient, string id, string callbackUrl, string token)
    {
        string confirmationUrl = $"{callbackUrl}?id={id}&token={token}";

        var emailDTO = new EmailDTO
        {
            Recipient = recipient,
            Subject = "Confirm new account at All My Gits",
            Body = $"Please use the following link to confirm the email <a href=\"{confirmationUrl}\">{confirmationUrl}</a>."
        };

        _emailClient.SendEmail(emailDTO);
    }

    public void SendPasswordResetEmail(string recipient, string id, string callbackUrl, string token)
    {
        string passwordResetUrl = $"{callbackUrl}?id={id}&token={token}";

        var emailDTO = new EmailDTO
        {
            Recipient = recipient,
            Subject = "Reset password at All My Gits",
            Body = $"Please use the following link to confirm the email <a href=\"{passwordResetUrl}\">{passwordResetUrl}</a>."
        };

        _emailClient.SendEmail(emailDTO);
    }
}
