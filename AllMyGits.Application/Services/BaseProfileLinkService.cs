﻿namespace AllMyGits.Application.Services;

using AllMyGits.Core.Entities;
using AllMyGits.Core.Interfaces.Repositories;
using AllMyGits.Core.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

public abstract class BaseProfileLinkService<TProfileLink> : IProfileLinkService<TProfileLink> where TProfileLink : BaseProfileLink
{
    protected readonly IProfileLinkRepository<TProfileLink> _profileLinkRepository;

    public BaseProfileLinkService(IProfileLinkRepository<TProfileLink> profileLinkRepository)
    {
        _profileLinkRepository = profileLinkRepository;
    }

    public async Task CreateAsync(TProfileLink profileLink)
    {
        _profileLinkRepository.Add(profileLink);
    }

    public async Task DeleteAsync(TProfileLink profileLink)
    {
        _profileLinkRepository.Remove(profileLink);
    }

    public async Task<TProfileLink> GetById(string id)
    {
        return _profileLinkRepository.Find(x => x.Id.Equals(id)).First();
    }

    public async Task<IEnumerable<TProfileLink>> GetByOwnerId(string ownerId)
    {
        return _profileLinkRepository.Find(x => x.Owner.Id.Equals(ownerId));
    }

    public async Task UpdateAsync(TProfileLink profileLink)
    {
        _profileLinkRepository.Update(profileLink);
    }
}
