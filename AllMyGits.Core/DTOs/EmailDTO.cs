﻿namespace AllMyGits.Core.DTOs;

public class EmailDTO
{
    public string Recipient { get; init; }
    public string Subject { get; init; }
    public string Body { get; init; }
}
