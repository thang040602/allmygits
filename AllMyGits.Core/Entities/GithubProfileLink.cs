﻿namespace AllMyGits.Core.Entities;

using System.ComponentModel.DataAnnotations;

public class GithubProfileLink : BaseProfileLink
{
    [Required]
    public string GithubUserName { get; set; }
}
