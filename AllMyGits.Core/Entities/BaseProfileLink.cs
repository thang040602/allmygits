﻿using System.ComponentModel.DataAnnotations;

namespace AllMyGits.Core.Entities
{
    public class BaseProfileLink
    {
        public long Id { get; set; }

        [Required]
        public User Owner { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }
    }
}
