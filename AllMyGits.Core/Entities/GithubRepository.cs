﻿namespace AllMyGits.Core.Entities;

public class GithubRepository : BaseRepository
{
    public string FullName { get; init; }
    public IEnumerable<string> Topics { get; init; }
}
