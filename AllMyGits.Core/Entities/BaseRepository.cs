﻿namespace AllMyGits.Core.Entities;

using AllMyGits.Core.Enums;

public class BaseRepository
{
    public string Name { get; init; }
    public string Description { get; init; }
    public RepositoryPlatform Platform { get; init; }
    public DateTime CreatedAt { get; init; }
    public DateTime UpdatedAt { get; init; }
}
