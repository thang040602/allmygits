﻿namespace AllMyGits.Core.ValueObjects;

using Microsoft.Extensions.Configuration;

public class EmailClientConfiguration
{
    public string SmtpServer { get; init; }
    public int Port { get; init; }
    public string UserName { get; init; }
    public string DisplayName { get; init; }
    public string Password { get; init; }

    public static EmailClientConfiguration GetConfiguration(IConfiguration configuration)
    {
        return new EmailClientConfiguration
        {
            SmtpServer = configuration["EmailClient:SmtpServer"],
            Port = int.Parse(configuration["EmailClient:Port"]),
            UserName = configuration["EmailClient:UserName"],
            DisplayName = configuration["EmailClient:DisplayName"],
            Password = configuration["EmailClient:Password"]
        };
    }
}
