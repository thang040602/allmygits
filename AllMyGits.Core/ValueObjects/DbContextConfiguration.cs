﻿using Microsoft.Extensions.Configuration;

namespace AllMyGits.Core.ValueObjects;

public class DbContextConfiguration
{
    public string Host { get; init; }
    public string DatabaseName { get; init; }
    public string UserName { get; init; }
    public string Password { get; init; }

    public static DbContextConfiguration GetConfiguration(IConfiguration configuration)
    {
        return new DbContextConfiguration
        {
            Host = configuration["DbContext:Host"],
            DatabaseName = configuration["DbContext:DatabaseName"],
            UserName = configuration["DbContext:UserName"],
            Password = configuration["DbContext:Password"]
        };
    }
}
