﻿namespace AllMyGits.Core.ValueObjects;

using Microsoft.Extensions.Configuration;

public class DomainNameConfiguration
{
    public string DomainName { get; init; }

    public static DomainNameConfiguration GetConfiguration(IConfiguration configuration)
    {
        return new DomainNameConfiguration
        {
            DomainName = configuration["DomainName"]
        };
    }
}
