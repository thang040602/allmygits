﻿using Microsoft.Extensions.Configuration;

namespace AllMyGits.Core.ValueObjects;

public class JwtConfiguration
{
    public string Secret { get; init; }
    public string Issuer { get; init; }
    public string Audience { get; init; }
    public int DaysExpiration { get; init; }

    public static JwtConfiguration GetConfiguration(IConfiguration configuration)
    {
        return new JwtConfiguration
        {
            Secret = configuration["Jwt:Secret"],
            Issuer = configuration["Jwt:Issuer"],
            Audience = configuration["Jwt:Audience"],
            DaysExpiration = int.Parse(configuration["Jwt:DaysExpiration"])
        };
    }
}
