﻿namespace AllMyGits.Core.ValueObjects;

using Microsoft.Extensions.Configuration;

public class GithubClientConfiguration
{
    public string BaseUrl { get; init; }
    public string BaseApiUrl { get; init; }
    public string ClientId { get; init; }
    public string ClientSecret { get; init; }
    public string State { get; init; }
    public string Scope { get; init; }

    public static GithubClientConfiguration GetConfiguration(IConfiguration configuration)
    {
        return new GithubClientConfiguration
        {
            BaseUrl = configuration["GithubClient:BaseUrl"],
            BaseApiUrl = configuration["GithubClient:BaseApiUrl"],
            ClientId = configuration["GithubClient:ClientId"],
            ClientSecret = configuration["GithubClient:ClientSecret"],
            State = configuration["GithubClient:State"],
            Scope = configuration["GithubClient:Scope"]
        };
    }
}
