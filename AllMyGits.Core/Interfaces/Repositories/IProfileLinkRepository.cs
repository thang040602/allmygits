﻿namespace AllMyGits.Core.Interfaces.Repositories;

using System.Linq.Expressions;

public interface IProfileLinkRepository<TProfileLink> where TProfileLink : class
{
    void Add(TProfileLink profileLink);
    void Update(TProfileLink profileLink);
    void Remove(TProfileLink profileLink);
    void RemoveRange(IEnumerable<TProfileLink> profileLinks);
    IQueryable<TProfileLink> Find(Expression<Func<TProfileLink, bool>> predicate);
}
