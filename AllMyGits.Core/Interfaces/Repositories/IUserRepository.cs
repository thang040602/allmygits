﻿namespace AllMyGits.Core.Interfaces.Repositories;

using AllMyGits.Core.Entities;

using Microsoft.AspNetCore.Identity;

using System.Linq.Expressions;

public interface IUserRepository
{
    Task AddAsync(User user, string password);
    Task RemoveAsync(User user);
    Task UpdateAsync(User user);
    Task ChangeEmailAsync(User user, string newEmail, string token);
    Task ChangePasswordAsync(User user, string currentPassword, string newPassword);
    IQueryable<User> Find(Expression<Func<User, bool>> predicate);
    Task<User> FindByEmailAsync(string email);
    Task<User> FindByUserNameAsync(string userName);
    Task<User> FindByIdAsync(string id);
    Task<SignInResult> PasswordSignInAsync(string userName, string password);
    Task<string> GeneratePasswordResetTokenAsync(User user);
    Task<string> GenerateEmailConfirmationTokenAsync(User user);
    Task<string> GenerateChangeEmailTokenAsync(User user, string newEmail);
    Task<IdentityResult> ConfirmEmailAsync(User user, string token);
    Task<IdentityResult> ResetPasswordAsync(User user, string token, string password);
}
