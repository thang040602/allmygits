﻿namespace AllMyGits.Core.Interfaces.Repositories;

using AllMyGits.Core.Entities;

public interface IGithubProfileLinkRepository : IProfileLinkRepository<GithubProfileLink>
{
}
