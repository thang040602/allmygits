﻿namespace AllMyGits.Core.Interfaces.Services;

using AllMyGits.Core.Entities;

public interface IGithubProfileLinkService : IProfileLinkService<GithubProfileLink>
{
    Task<IEnumerable<GithubRepository>> GetRepositories(string ownerId);
    Task CreateAsync(string ownerId, string githubUserName);
}
