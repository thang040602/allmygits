﻿namespace AllMyGits.Core.Interfaces.Services;

using AllMyGits.Core.Entities;

public interface IProfileLinkService<TProfileLink> where TProfileLink : BaseProfileLink
{
    Task CreateAsync(TProfileLink profileLink);
    Task UpdateAsync(TProfileLink profileLink);
    Task DeleteAsync(TProfileLink profileLink);
    Task<IEnumerable<TProfileLink>> GetByOwnerId(string ownerId);
    Task<TProfileLink> GetById(string id);
}
