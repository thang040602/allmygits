﻿namespace AllMyGits.Core.Interfaces.Services;

public interface IEmailService
{
    void SendConfirmationEmail(string recipient, string id, string callbackUrl, string token);
    void SendPasswordResetEmail(string recipient, string id, string callbackUrl, string token);

}
