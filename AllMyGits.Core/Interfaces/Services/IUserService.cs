﻿namespace AllMyGits.Core.Interfaces.Services;

using AllMyGits.Core.Entities;

public interface IUserService
{
    Task AddAsync(string userName, string email, string password, string firstName = null, string lastName = null);
    Task UpdateAsync(string id, string? userName, string? firstName, string? lastName);
    Task RemoveAsync(string id);
    Task<User> FindByIdAsync(string id);
    Task<User> FindByUserNameAsync(string userName);
    Task<User> FindByEmailAsync(string email);
    Task<string> AuthenticateAsync(string userNameOrEmail, string password);
    Task<bool> ConfirmEmailAsync(string id, string token);
    Task ForgetPasswordAsync(string email, string callbackUrl);
    Task ResetPasswordAsync(string id, string password, string token);
}
