﻿namespace AllMyGits.Core.Interfaces.Clients;

using AllMyGits.Core.DTOs;

public interface IEmailClient
{
    void SendEmail(EmailDTO message);
}
