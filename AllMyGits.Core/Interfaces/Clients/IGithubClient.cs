﻿namespace AllMyGits.Core.Interfaces.Clients;

using AllMyGits.Core.Entities;

public interface IGithubClient
{
    Task<IQueryable<GithubRepository>> GetRepositoriesAsync(string userName);
}
