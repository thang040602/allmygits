namespace AllMyGits.Test;

using AllMyGits.Application.Services;
using AllMyGits.Core.Entities;
using AllMyGits.Core.Interfaces.Repositories;
using AllMyGits.Core.Interfaces.Services;
using AllMyGits.Core.ValueObjects;
using AllMyGits.Infrastructure.Repositories;
using AllMyGits.Presentation.Api.Controllers;
using AllMyGits.Presentation.Api.ViewModels.User;
using Bogus;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;

[TestFixture]
public class UserRegisterTests
{
    private SqliteConnection sqliteConnection;
    private AllMyGitsDbContext dbContext;
    private UserManager<User> userManager;
    private SignInManager<User> signInManager;
    private IUserService userService;
    private IUserRepository userRepository;
    private UserController userController;

    [SetUp]
    public void SetUp()
    {
        IServiceCollection serviceCollection = new ServiceCollection();

        sqliteConnection = new SqliteConnection("DataSource=:memory:");
        serviceCollection.AddDbContext<AllMyGitsDbContext>(options => options.UseSqlite(sqliteConnection));

        // Add Identity using in memory database
        serviceCollection.AddIdentity<User, IdentityRole>(options =>
        {
            options.User.RequireUniqueEmail = true;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequiredUniqueChars = 0;
            options.Password.RequireDigit = false;
            options.Password.RequireUppercase = false;
            options.Password.RequireLowercase = false;
            options.Password.RequiredLength = 6;
        }).AddEntityFrameworkStores<AllMyGitsDbContext>().AddDefaultTokenProviders();

        serviceCollection.AddLogging(options =>
        {
            options.ClearProviders();
            options.AddConsole();
        });

        var serviceProvider = serviceCollection.BuildServiceProvider();

        dbContext = serviceProvider.GetService<AllMyGitsDbContext>();
        dbContext.Database.OpenConnection();
        dbContext.Database.EnsureCreated();

        // Get UserManager and SignInManager
        signInManager = serviceProvider.GetService<SignInManager<User>>();
        userManager = serviceProvider.GetService<UserManager<User>>();

        var emailServiceMock = new Mock<IEmailService>();
        emailServiceMock.Setup(x => x.SendConfirmationEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));

        userRepository = new UserRepository(signInManager, userManager);
        userService = new UserService(userRepository, emailServiceMock.Object, null, new DomainNameConfiguration { DomainName = "" });
        userController = new UserController(userService);
    }

    [TearDown]
    public void TearDown()
    {
        dbContext.Database.EnsureDeleted();
        dbContext.Dispose();
        sqliteConnection.Close();
    }

    [Test]
    public async Task RegisterAsync_Succeed()
    {
        var fakerRequest = new Faker<RegisterViewModel>()
            .RuleFor(x => x.FirstName, f => f.Person.FirstName)
            .RuleFor(x => x.LastName, f => f.Person.LastName)
            .RuleFor(x => x.Email, (f, x) => f.Internet.Email(x.FirstName, x.LastName))
            .RuleFor(x => x.UserName, (f, x) => f.Internet.UserName(x.FirstName, x.LastName))
            .RuleFor(x => x.Password, f => f.Internet.Password());
        var fakeRequest = fakerRequest.Generate();

        await userController.RegisterAsync(fakeRequest);

        Assert.That(dbContext.Set<User>().Count(), Is.EqualTo(1));
    }

    [Test]
    public async Task RegisterAsync_InvalidPassword_Unsucceed()
    {
        var fakerRequest = new Faker<RegisterViewModel>()
            .RuleFor(x => x.FirstName, f => f.Person.FirstName)
            .RuleFor(x => x.LastName, f => f.Person.LastName)
            .RuleFor(x => x.Email, (f, x) => f.Internet.Email(x.FirstName, x.LastName))
            .RuleFor(x => x.UserName, (f, x) => f.Internet.UserName(x.FirstName, x.LastName))
            .RuleFor(x => x.Password, f => f.Internet.Password(5));
        var fakeRequest = fakerRequest.Generate();

        Assert.ThrowsAsync<SystemException>(async () => await userController.RegisterAsync(fakeRequest));
        Assert.That(dbContext.Set<User>().Count(), Is.EqualTo(0));
    }
}